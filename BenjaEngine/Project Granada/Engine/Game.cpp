#include "stdafx.h"
#include <d3d9.h> //Busca el header de directx en los path
#include "Game.h"
#include "FVF.h"
#include "Texture.h"
#include "Input.h"
#include "Camera.h"
#include <assimp/Importer.hpp>r

#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto
#include "GameInterface.h"

#include "SceneImporter.h"


/*




*/


Game* Game::instance = nullptr;

//--------------------------
//TESTING ASSIMP IMPORTER


Game::Game()
{
	Game::instance = this; //Game se instancia en si mismo; volviendose global.
}
Game::~Game()
{
}
HWND Game::gethWnd()
{
	return hWnd;
}
HINSTANCE Game::gethInstance()
{
	return _hInstance;
}
Game * Game::getInstance()
{
	if (instance == 0)
	{
		instance = new Game();
	}
	return instance;
}
LPDIRECT3DDEVICE9 Game::GetDevice()
{
	return dev;
}
void Game::FrameEvent(){}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

/*virtual void Game::LoadContent()
{

}
*/
void Game::Run(_In_     HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_     LPTSTR    lpCmdLine,
	_In_     int       nCmdShow, GameInterface* comingInterface)
{
	//SceneImporter *sceneImporter = new SceneImporter();
	//sceneImporter->ImportScene("bspTest4.dae");

	//Creamos la clase de la ventana
	WNDCLASSEX wcex;
	_hInstance = hInstance;
	//Iniciamos sus valores en 0
	ZeroMemory(&wcex, sizeof(WNDCLASSEX));


	wcex.cbSize = sizeof(WNDCLASSEX); //Tama�o en bytes
	wcex.style = CS_HREDRAW | CS_VREDRAW; //Estilo de la ventana
	wcex.lpfnWndProc = WndProc; //Funcion de manejo de mensajes de ventana
	wcex.hInstance = hInstance; //Numero de instancia
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW); //Cursor del mouse
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); //Color de fondo
	wcex.lpszClassName = L"GameWindowClass"; //Nombre del tipo (clase) de ventana

											 //Registro mi tipo de ventana en windows
	RegisterClassEx(&wcex);

	//Creo la ventana, recibiendo el numero de ventana
	hWnd = CreateWindowEx(0, //Flags extra de estilo
		L"GameWindowClass", //Nombre del tipo de ventana a crear
		L"Granada", //Titulo de la barra
		WS_OVERLAPPEDWINDOW, //Flags de estilos
		0, //X
		0, //Y
		640, //Ancho
		480, //Alto
		NULL, //Ventana padre
		NULL, //Menu
		hInstance, //Numero de proceso
		NULL); //Flags de multi ventana

	ShowWindow(hWnd, nCmdShow); //Muestro la ventana
	UpdateWindow(hWnd); //La actualizo para que se vea

						//Me comunico con directx por una interfaz, aca la creo
	LPDIRECT3D9 d3d = Direct3DCreate9(D3D_SDK_VERSION);

	//Creo los parametros de los buffers de dibujado (pantalla)
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));
	d3dpp.Windowed = true;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = hWnd;

	//Creo la interfaz con la placa de video
	//LPDIRECT3DDEVICE9 dev; //SE CREA ACA O EN EL H?
	d3d->CreateDevice(D3DADAPTER_DEFAULT, //Cual placa de video
		D3DDEVTYPE_HAL, //Soft o hard
		hWnd, //Ventana
		D3DCREATE_HARDWARE_VERTEXPROCESSING, //Proceso de vertices por soft o hard
		&d3dpp, //Los parametros de buffers
		&dev); //El device que se crea

	dev->SetRenderState(D3DRS_LIGHTING, FALSE);

	GameInterface* theInterface = comingInterface; //Un puntero de GameInterface con una instancia del juego que se desarrolle.
	theInterface->SetRoot(root);
	theInterface->LoadContent();

	//Camera *principalCamera = new Camera();

	while (true)
	{


		MSG msg;

		//Saco un mensaje de la cola de mensajes si es que hay
		//sino continuo
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		if (msg.message == WM_QUIT)
		{
			break;
		}
		//Actualizar
		//camara
		//principalCamera->Update();

		dev->Clear(0, NULL, D3DCLEAR_TARGET, /*D3DCOLOR_XRGB(255, 0, 0)*/ D3DCOLOR_ARGB(0, 200, 0, 100), 1.0f, 0);
		dev->BeginScene();
		theInterface->FrameEvent();
		dev->EndScene();
		dev->Present(NULL, NULL, NULL, NULL);

	}

	dev->Release();
	d3d->Release();
	/*
	delete engineObject1;
	delete engineObject2;
	delete engineObject3;
	delete engineObject4;
	delete engineObject5;
	delete engineObjectControled;
	*/
	//delete principalCamera;
	//delete theOnlyMesher;
	//delete firstTexture;
	//delete woodTexture;
	//delete intVectorInputUp;
	//delete intVectorInputLeft;
	//delete intVectorInputRight;
	//delete intVectorInputDown;
	//delete theInput;
}

//Manejo de mensajes por ventana
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_DESTROY)
	{
		//Si destruyeron esta ventana (cerraron) le pido
		//a windows que cierre la app
		PostQuitMessage(0);
		return 0;
	}

	//Si no maneje el mensaje antes, hago el comportamiento por defecto
	return DefWindowProc(hWnd, message, wParam, lParam);
}