#ifndef BSP_H
#define BSP_H
#include "Composite.h"
#include <list>
class IMPORTEXPORT BSP
{
private:
	list<Composite*> allObjects;
public:
	BSP();
	~BSP();
	void LoadPlane(Composite* plane);
};
#endif