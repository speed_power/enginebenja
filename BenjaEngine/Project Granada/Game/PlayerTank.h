#pragma once
#include <iostream>
#include "../Engine/Composite.h"
#include "../Engine/Input.h"
//#include <map>
//#include <vector>
using namespace std;

class PlayerTank : public Composite
{
private:
	int x;
	int y;
	int lives;
	static PlayerTank* instance;
	PlayerTank(); //Singleton
public:
	static PlayerTank* GetInstance();
	//~PlayerTank();
	void Controls(/*Tecla presionada*/);
	void Move();
	void Aim(/*Poscision del mouse*/);
	void fire();
	void Damage();
	void Death(); //preguntar
};

