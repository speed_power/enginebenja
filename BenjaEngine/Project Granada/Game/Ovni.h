#ifndef OVNI_H
#define OVNI_H
#include <iostream>
#include "../Engine/Composite.h"
class Ovni :
	public Composite
{
private:
	float x;
	float y;
	bool lookingRight;
	bool lookingDown;
public:
	Ovni();
	~Ovni();
	void HorizontalBounce();
	void VerticalBounce();
	bool GetLookingRight();
	bool GetLookingDown();
	float GetX();
	float GetY();
	void SetX(float _x);
	void SetY(float _y);
};
#endif
